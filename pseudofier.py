import re
import sys

def fatal_error(message):
	print("Fatal error: %s" % message)
	input("Press any button to exit...")
	exit()

def to_line(s):
    s = s.strip()
    
    if s[-1] == ':':
        s = s[:-1]
    
    return r'\li ' + s

def convert_len(s):
    vals = re.split(r"(?<!\w)len\((.*?)\)", s)

    output = ""

    for val in vals[:-1]:
        output += val + "|"

    output += vals[-1]
    return output
    
def convert(lines):
    output = r'\begin{codebox}'+"\n"

    tabs = 1

    name = re.search(r'def (.*?)\(', lines[0])
    para = re.search(r'\((.*?)\)', lines[0])
    output += r'  \Procname{{$\proc{{{0}}}{1}$}}'.format(name.group(1), para.group()) + "\n"

    lastLine = len(lines) - 1

    while not lines[lastLine] or lines[lastLine].isspace():
        lastLine -= 1

    for x in lines[1:lastLine+1]:
        if 'else ' in x:
            y = to_line(x.replace('elif ', r'\Else '))
        elif 'elif ' in x:
            y = to_line(x.replace(r'elif ', r'\Else \If '))
        elif 'if ' in x:
            y = to_line(x.replace(r'if ', r'\If '))
        elif 'while ' in x:
            y = to_line(x.replace(r'while ', r'\While '))
        elif 'for ' in x:
            y = r'  \li '+x.replace(r'for ', r'\For ').split(' in ')[0]

            if('range(' in x):
                para = re.search(r'\((.*)\)', x).group(1).split(',')
                if len(para) == 1:
                    y += '=0 to {0}'.format(para[0])
                elif len(para) == 2:
                    y += '={0} to {1}'.format(para[0], para[1])
                elif len(para) == 3:
                    if(int(para[2]) > 0):
                        if(int(para[2]) == 1):
                            y += '={0} to {1}'.format(para[0], para[1])
                        else:
                            y += '={0} to {1} by {2}'.format(para[0], para[1], para[2])
                    else:
                        if(int(para[2]) == -1):
                            y += '={0} downto {1}'.format(para[0], para[1])
                        else:
                            y += '={0} downto {1} by {2}'.format(para[0], para[1], para[2][:-1])
        else:
            y = r'  \li '+x

        if x.strip():
            tab_match = re.search(r'^( *)', x)

            if tab_match == None:
                newtabs = 0
            else:
                newtabs = len(tab_match.group()) // 4

            tab_diff = newtabs - tabs

            if tab_diff != 0:
                for i in range(tab_diff):
                    output += r'\Do '
                for i in range(-tab_diff):
                    output += r'\End '

                tabs = newtabs
        
        output += y+"\n"
    
    output += r'\end{codebox}'
    output = convert_len(output)
    return output

#i = input('Python file: ')

file = sys.argv[1] if len(sys.argv) > 1 else None

if file == None:
	fatal_error("No file given.")

f = open(file, 'r').read()

methods = [("def " + m).split("\n") for m in f.split("def ") if len(m) > 0]

out = ""
for m in methods:
    out += convert(m) + "\n\n"

print(out)